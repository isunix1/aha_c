/*
 * =====================================================================================
 *
 *       Filename:  while_star1.c
 *
 *    Description:  print the * char
 *
 *        Version:  1.0
 *        Created:  07/15/2014 13:55:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int a;
  a = 1;
  while(a <= 15){
    printf("*");
    if (a%5 == 0){
      printf("\n");
    }

    a = a + 1;
  }
}
