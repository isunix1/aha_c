/*
 * =====================================================================================
 *
 *       Filename:  swap_ok.c
 *
 *    Description:  swap successfully
 *
 *        Version:  1.0
 *        Created:  07/15/2014 13:24:08
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int a, b, t;
  scanf("%d %d", &a, &b);
  t = a;
  a = b;
  b = t;

  printf("%d %d\n", a, b);
}


