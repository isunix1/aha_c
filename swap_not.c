/*
 * =====================================================================================
 *
 *       Filename:  swap_not.c
 *
 *    Description:  swap does not complete ok
 *
 *        Version:  1.0
 *        Created:  07/15/2014 13:19:02
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int a, b;
  scanf("%d %d", &a, &b);

  a = b;
  b = a;

  printf("%d %d\n", a, b);

  return 0;
}
