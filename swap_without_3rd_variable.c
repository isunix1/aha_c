/*
 * =====================================================================================
 *
 *       Filename:  swap_without_3rd_variable.c
 *
 *    Description:  swap without adding a new variable
 *
 *        Version:  1.0
 *        Created:  07/15/2014 13:26:40
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int a, b;
  scanf("%d %d", &a, &b);
  a = b - a;
  b = b - a;
  a = b + a;

  printf("%d %d\n", a, b);
  return 0;
}
