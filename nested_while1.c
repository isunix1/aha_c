/*
 * =====================================================================================
 *
 *       Filename:  nested_while1.c
 *
 *    Description:  nested while loop 1 example
 *
 *        Version:  1.0
 *        Created:  07/15/2014 14:01:03
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <stdio.h>

int main(){
  int a, b;
  a = 1;

  while(a < 3){
    b = 1;
    while(b < 5){
      printf("*");
      b = b + 1;
    }

    printf("\n");
    a = a + 1;
  }

  return 0;
}
